//============================================================================
// Name        : elevator.cpp
// Author      : kd
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <set>
#include <fstream>
#include <string>


using namespace std;

class evelavtor
{
private:
	enum class Modes {ModeA = 'A', ModeB ='B'};
	Modes operationType;
	vector<vector<uint>> floors;
	vector<vector<uint>> floorsTraveledB;
	vector<uint> distances;

	uint uintAbs(const uint a, const uint b) const
	{
		return ((a > b) ? (a - b) : (b - a));
	}
	void AddFloorSet(const bool up, const std::set<uint> & temp, vector<uint> & fl)
	{
		if(temp.empty()) return;

		if(up)
		{
			if(fl[fl.size() - 1] == *temp.begin())
			{
				fl.pop_back();
			}
			std::copy(temp.begin(), temp.end(), std::back_inserter(fl));
		}
		else {
			if(fl[fl.size() - 1] == *(temp.rbegin()))
			{
				fl.pop_back();
			}
			std::copy(temp.rbegin(), temp.rend(), std::back_inserter(fl));
		}
	}
	void calculateDistancesB()
	{
		for(auto & line : floors)
		{
			uint currFloor {line[0]};
			std::set<uint> temp;
			floorsTraveledB.emplace_back(1,currFloor);
			uint sum {0 }, index {1};
			bool up = line[index] < line[index + 1];
			uint startFloor{line[index]}, endFloor{line[index + 1]};
			for(; index < line.size() - 1 ; index += 2)
			{

				if(up == (line[index] < line[index + 1]))
				{
					temp.insert(line[index]);
					temp.insert(line[index + 1]);
					if(up) {
						if(line[index] < startFloor) startFloor = line[index];
						if(line[index + 1] > endFloor) endFloor = line[index + 1];
					}
					else {
						if(line[index] > startFloor) startFloor = line[index];
						if(line[index + 1] < endFloor) endFloor = line[index + 1];
					}
				}
				else {
					sum += uintAbs(currFloor,startFloor);
					sum += uintAbs(startFloor,endFloor);
					AddFloorSet(up, temp, floorsTraveledB[floorsTraveledB.size() - 1]);
					temp.clear();
					temp.insert(line[index]);
					temp.insert(line[index + 1]);
					currFloor = endFloor;
					startFloor = line[index];
					endFloor = line[index + 1];
					up = line[index] < line[index + 1];
				}
			}
			AddFloorSet(up, temp, floorsTraveledB[floorsTraveledB.size() - 1]);
			sum += uintAbs(currFloor,startFloor);
			sum += uintAbs(startFloor,endFloor);
			distances.push_back(sum);
		}
	}
	void calculateDistancesA()
	{
		for(auto & line : floors)
		{
			uint prevFloor {line[0]};
			uint sum {0};
			for(auto & floor: line)
			{
				sum += uintAbs(floor,prevFloor);
				prevFloor = floor;
			}
			distances.push_back(sum);
		}
	}

public:
	void CalcDist()
	{
		if(operationType == Modes::ModeA)
		{
			calculateDistancesA();
		}
		else if(operationType == Modes::ModeB)
		{
			calculateDistancesB();
		}
	}
	void setOpType(char tp)
	{
		operationType = static_cast<Modes>(tp);
	}
	void addFloor(const uint line, const uint floor)
	{
		if(floors.size() == line)
		{
			floors.emplace_back(1,floor);
		}
		else {
			floors[line].push_back(floor);
		}
	}
	void printDistances() const
	{
		uint index = 0;
		cout<<"Mode "<<static_cast<char>(operationType)<<endl;
		if(operationType == Modes::ModeA)
		{
			for(auto & line : floors)
			{

				for(auto & floor: line)
				{
					cout<<floor<<" ";
				}
				cout<<"("<<distances[index++]<<")"<<endl;
			}
		}
		else {
			for(auto & line : floorsTraveledB)
			{

				for(auto & floor: line)
				{
					cout<<floor<<" ";
				}
				cout<<"("<<distances[index++]<<")"<<endl;
			}
		}
	}
};

bool ReadFile(string & fileName, evelavtor & el)
{
	bool retVal = false;
	cout<<fileName<<endl;
	std::ifstream inputFile( fileName);
	if(inputFile) {
		retVal = true;
		std::string line;
		uint ln = 0;
		while (std::getline(inputFile, line))
		{
			uint sum{0};
			for(char t: line)
			{
				if(t >= '0' && t <= '9')
				{
					if(sum == 0) {
						sum = t - 48;
					}
					else {
						sum = 10 * sum + (t - 48);
					}
				}
				else {
					el.addFloor(ln,sum);
					sum = 0;
				}
			}
			el.addFloor(ln,sum);
			ln++;
		}
	}
	return retVal;
}

void InputData(std::string & inFile, char & type, evelavtor & el)
{
	cout<<"Input name of the file: ";
	bool readSuccess = false;
	while(!readSuccess)
	{
		cin>>inFile;
		readSuccess = ReadFile(inFile,el);
		if(!readSuccess) {
			cout<<"File not found! Enter file name: ";
		}
	}

	cout<<"Enter mode (A or B): ";
	cin>>type;
	while(type != 'A' && type != 'B')
	{
		cout<<"Invalid selection!. Enter mode (A or B): ";
		cin>>type;
	}
}

int main() {
	std::string inputFile;
	char mode;
	evelavtor el;
	InputData(inputFile,mode, el);
	el.setOpType(mode);
	el.CalcDist();
	el.printDistances();
	return 0;
}
